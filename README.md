# FAQ

A bunch of scripts and data files intended to build a Frequent Asked Questions document (and some useful tools) to Flight of Nova.

It's a companion for the ongoing discussion on Steam [(Yet Another) F.A.Q. for Flight Of Nova (WiP)](https://steamcommunity.com/app/1069190/discussions/0/4286937147060127836/).

By Lisias.


## Configuration Management

I will try to avoid any "professional" tooling here (other than git). But some assembling is necessary - and batteries are not included.

So, in order to play with this, you will need to follow these instructions:

1. Prepare a Python running environment
2. Clone this repository into your rig

I will replicate exactly what I do on my rig. Feel free to adapt to your needs.

### Preparing the running environment

You need to have Python installed, preferably 3.12 or at very least 3.11 - avoid creating new projects with older Pythons.

You will need to have `virtualenv` installed too!

Installing Python and `virtualenv` are highly depending on OS (and sometimes, with multiple options). 

#### MacOS with `macports`

Since I'm running MacOS with `macports`, this is what I did:

On a terminal window:

```
sudo port install python312
sudo port install py-virtualenv
sudo port install git
```

#### Windows with `cygwin`

If you don't want to meddle with the Linus Subsystem for Windows, you can toy with `cygwin`. It's a fairly complex tool to emulate a POSIX environment under Windows, but it's useful nevertheless

WiP.

#### For everybody

From this point, everything will be similar, changing only if you decide to use Python directly under Windows.


On a terminal window:

```
mkdir ~/Workspaces
mkdir ~/Workspaces/FoN
```

Now we need to create the python running environment for this project. It's better to have a container (something like a sandbox) for it so you don't risk screwing things up to other projects (or vice versa) by installing something that could cause some conflict (and it happens). So:

```
cd ~/Workspaces/FoN
virtualenv .python
. .python/bin/activate
pip install scipy
```

You **will** want to create a shortcut to 	`IDLE`, the "Python's Integrated Development and Learning Environment", otherwise you will end up using `vim` or some other black magic for editing your python programs. :) 

If you are on a UNIX, create the following file and save it on `~/Workspaces/FoN/python/bin/idle`. You will need to make it runnable by using the command `chmod +x ~/Workspaces/FoN/python/bin/idle`

```bash
#!/usr/bin/env bash
python -m idlelib
```

On Windows, I will update this document later.

And finally, this will clone ("download") all the source code and other assets into your local directory:

```
cd ~/Workspaces/FoN
git clone https://gitlab.com/net-lisias-fon/faq.git FAQ
```

And now some convenience scripts:

On `~/Workspaces/FoN`, save the following as a file called `.profile`:

```bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
for d in FAQ ; do
	export PYTHONPATH=${SCRIPT_DIR}/${d}/src:${PYTHONPATH}
done
```

On `~/Workspaces/FoN`, save the following as a file called `start`:

```bash
#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
. ${SCRIPT_DIR}/.python/bin/activate
. ${SCRIPT_DIR}/.profile
idle
```

Set it as runnable by using the command `chmod +x ~/Workspaces/FoN/start`


And you are set!


## How to use

The easiest way to start toying is to run the command `start`, as follows:

```bash
cd ~/Workspaces/FoN/FAQ
./start
```

But if you want to do it by hand, you can also:

```bash
cd ~/Workspaces/FoN/FAQ
. ~/Workspaces/FoN/.python/bin/activate
. ~/Workspaces/FoN/.profile
```

And from now on you can just run the programs or call the editor:

```bash
cd ~/Workspaces/FoN/FAQ
python src/NVA-3.py
idle
```

## Licensing

* This work is licensed as follows:
	+ [GPL 2.0](https://www.gnu.org/licenses/gpl-2.0.txt). See [here](./LICENSE.GPL-2_0)
		+ You are free to:
			- Use : unpack and use the material in any computer or device
			- Redistribute : redistribute the original package in any medium
			- Adapt : Reuse, modify or incorporate source code into your works (and redistribute it!) 
		+ Under the following terms:
			- You retain any copyright notices
			- You recognize and respect any trademarks
			- You don't impersonate the authors, neither redistribute a derivative that could be misrepresented as theirs.
			- You credit the author and republish the copyright notices on your works where the code is used.
			- You relicense (and fully comply) your works using GPL 2.0
				- Please note that upgrading the license to GPLv3 **IS NOT ALLOWED** for this work, as the author **DID NOT** added the "or (at your option) any later version" on the license.
			- You don't mix your work with GPL incompatible works.

## References

* Flight of Nova
	+ [Homepage](https://flight-of-nova.com/)
	+ [Steam](https://store.steampowered.com/app/1069190/Flight_Of_Nova/)
* Steam Discussions:
	+ [(Yet Another) F.A.Q. for Flight Of Nova (WiP)](https://steamcommunity.com/app/1069190/discussions/0/4286937147060127836/).

