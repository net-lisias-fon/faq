from math import sin, sqrt, radians

g = 9.81

def S(v0, alpha):
	global g
	return ((v0*v0)/g) * sin(2*alpha)

def h(v0, alpha):
	global g
	return ((v0*v0)/(2*g))* sin(alpha)**2

def t(v0, alpha):
	global g
	return (2*v0*sin(alpha))/g

def v0(S, alpha):
	"""
	S = ((v0*v0)/g) * sin(2*alpha)
	S / sin(2*alpha) = (v0*v0)/g
	(S / sin(2*alpha)) * g = v0*v0
	v0 = sqrt((S / sin(2*alpha)) * g)
	"""
	return sqrt((S / sin(2*alpha)) * g)

def attempt_1():
	my_S = 15000000
	my_alpha = radians(45)
	calculated_v0 = v0(my_S, my_alpha)
	calculated_h = h(calculated_v0, my_alpha)
	calculated_t = t(calculated_v0, my_alpha)
	print ("v0 = {0}".format(calculated_v0))
	print ("h = {0}".format(calculated_h))
	print ("t = {0}".format(calculated_t))
	
def attempt_2():
	my_v0 = 7888
	mt_h = 110000
	my_alpha = radians(1)
	calculated_S = S(my_v0, my_alpha)
	calculated_t = t(my_v0, my_alpha)
	print ("S = {0}".format(calculated_S))
	print ("t = {0}".format(calculated_t))


if "__main__" == __name__ :
	attempt_2()
	
