import sys
import matplotlib.pyplot as plt
import mplcursors
import numpy as np
import math

UNIVERSE = None

def translate_coord_base(base:dict, r:float) -> tuple:
	x = r * math.cos(math.radians(base['LAT'])) * math.cos(base['LONG'])
	y = r * math.cos(math.radians(base['LAT'])) * math.sin(base['LONG'])
	z = r * math.sin(math.radians(base['LAT']))
	return (base, x, y, z)

def get_bases_nxyz(systems:dict, planet:str, r:float):
	bases = systems[planet]['BASE']
	result = list()
	for b in bases:
		base = bases[b]
		result.append(translate_coord_base(base, r))
	return result

COLORS = {"*": "red", "NP": "red", "SP": "red", "Player": "crimson", "Launch Site": "blue", "Power Station": "orange", "Construct": "grey"}
MARKERS = {"*": "*", "NP": "D", "SP": "d", "Player": "+", "Launch Site": "^", "Power Station": "v", "Construct": ".", "Selected": "v"}
def plot_bases(bases:list, r:float):
	# creating figure
	plt.style.use('seaborn-v0_8-whitegrid')
	fig = plt.figure()
	ax = fig.add_subplot(projection='3d')
	ax.set_box_aspect([1,1,1])

	def draw_sphere(ax, r:float):
		# Draw a sphere to mimic a planet
		u = np.linspace(0, 2 * np.pi, r)
		v = np.linspace(0, np.pi, r)
		x = r * np.outer(np.cos(u), np.sin(v))
		y = r * np.outer(np.sin(u), np.sin(v))
		z = r * np.outer(np.ones(np.size(u)), np.cos(v))
		ax.plot_surface(x, y, z, rcount=18, ccount=21, alpha=0.1)
	draw_sphere(ax,r)

	# creating the plot
	for i in range(0, len(bases)):
		label = "{0} {1:.2f}°;{2:.2f}°".format(bases[i][0]['NAME'],bases[i][0]['LONG'],bases[i][0]['LAT'])
		role = bases[i][0]['ROLE']
		xs = bases[i][1]
		ys = bases[i][2]
		zs = bases[i][3]
		ax.scatter(xs, ys, zs, c=COLORS[role], marker=MARKERS[role], label=label)

	crs = mplcursors.cursor(ax, hover=True)
	crs.connect("add", lambda sel: sel.annotation.set_text(repr(sel.artist.get_label())))

	# setting title and labels
	ax.set_title("NVA31 Bases")
	ax.set_zlabel('Polar Axis')
	ax.grid(False)

	# displaying the plot
	plt.show()

def __forge_base(name:str, role:str, long:float, lat:float, alt:float) -> dict:
	r = dict()
	r['NAME'] = name
	r['ROLE'] = role
	r['LONG'] = long
	r['LAT'] = lat
	r['ALT'] = alt
	return r

def plot(size:float, player:tuple = None):
	bases_coords = get_bases_nxyz(UNIVERSE.SYSTEMS['NOREN']['PLANET'], 'NVA-31', size)

	bases_coords.append(translate_coord_base(__forge_base("Null Island", "*", 0, 0, 0), size))
	bases_coords.append(translate_coord_base(__forge_base("North Pole", "NP", 0, 90, 0), size*1.2))
	bases_coords.append(translate_coord_base(__forge_base("South Pole", "SP", 0, -90, 0), size*1.2))

	if player:
		bases_coords.append(translate_coord_base(__forge_base(player[0], player[1], player[2], player[3], player[4]), size))

	plot_bases(bases_coords, int(size))


def main(args:list):
	global UNIVERSE
	if len(args) < 1:
		import NVA31 as NVA3
	elif 'basic' == args[0].lower():
		import NVA31 as NVA3
	else:
		import NVA31_spoiler as NVA3
	UNIVERSE = NVA3

	UNIVERSE.build()
	size = UNIVERSE.SYSTEMS['NOREN']['PLANET']['NVA-31']['r']/1000

	player = None
	if len(args) > 1:
		long = float(args[1])
		lat = float(args[2])
		alt = size*1.1
		if len(args) > 3:
			alt = float(args[3])
		player = ("Player", "Player", long, lat, alt)
	plot(size, player)

if "__main__" == __name__ :
	main(sys.argv[1:])
	print ('End of line.')
