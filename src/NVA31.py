import sys
from scipy.constants import pi as PI, gravitational_constant as Gc
import pprint

UNIT_PLANET = {
		'r': 'km',
		'C': 'km',
		'1LAT': 'km',
		'1LONG': 'km',
		'Mass': 'kg',
	}
UNIT_BASE = {
		'ALT': 'm',
		'LONG': '°',
		'LAT': '°',
	}
UNIT_STATION = {
		'ALT': 'km',
		'INC': '°',
		'ECC': '',
	}
RADIUS = 6302000
DIAMETER = 2*RADIUS
CIRCUMFERENCE = 2*PI*RADIUS
LAT = CIRCUMFERENCE / 360
LONG = CIRCUMFERENCE / 360

SYSTEMS = dict()

REF_BECCA_ALT = 188
REF_BECCA_RADIUS = RADIUS + REF_BECCA_ALT
REF_BECCA_Vorb = 7838

def _build_nss() -> dict:
	global SYSTEMS
	nss = SYSTEMS['NOREN'] = dict()
	nss['NAME'] = "Noren Star System"
	nss['PLANET'] = dict()
	return nss

def _build_nva3(system:dict) -> dict:
	nva3 = system['PLANET']['NVA-31'] = dict()
	nva3['NAME'] = "NVA-31"
	nva3['r'] = RADIUS
	nva3['C'] = CIRCUMFERENCE
	nva3['1LAT'] = LAT
	nva3['1LONG'] = LONG
	nva3['Mass'] = (REF_BECCA_Vorb^2) / (Gc/(1000*REF_BECCA_RADIUS))
	return nva3

def _build_station(planet:dict, name:str, role:str, inc:float, ecc:float, alt:float) -> dict:
	if 'STATION' not in planet: planet['STATION'] = dict()
	base = planet['STATION'][name] = dict()
	base['INC'] = inc
	base['ECC'] = ecc
	base['ALT'] = alt
	base['NAME'] = name
	base['ROLE'] = role
	return base

def _build_base(planet:dict, name:str, role:str, long:float, lat:float, alt:float) -> dict:
	if 'BASE' not in planet: planet['BASE'] = dict()
	base = planet['BASE'][name] = dict()
	base['LONG'] = long
	base['LAT'] = lat
	base['ALT'] = alt
	base['NAME'] = name
	base['ROLE'] = role
	return base

def __print_base(base:dict):
	my_print(3, "Base {0} ({1})", base['NAME'], base['ROLE'])
	for d in base.keys():
		if d in ['NAME', 'ROLE']: continue
		if type(base[d]) is dict: continue
		my_print(4, "{0} : {1:,.2f} {2}", d, base[d], UNIT_BASE[d])

def __print_station(station:dict):
	my_print(3, "Station {0} ({1})", station['NAME'], station['ROLE'])
	for d in station.keys():
		if d in ['NAME', 'ROLE']: continue
		if type(station[d]) is dict: continue
		my_print(4, "{0} : {1:,.2f} {2}", d, station[d], UNIT_STATION[d])

def __print_planet(planet:dict):
	my_print(1, "Planet {0}", planet['NAME'])
	for d in planet.keys():
		if 'NAME' == d: continue
		if type(planet[d]) is dict: continue
		my_print(2, "{0} : {1:,.2f} {2}", d, planet[d]/1000, UNIT_PLANET[d])
	if 'BASE' in planet:
		my_print(2, "Bases:")
		for b in sorted(planet['BASE'].keys()):
			__print_base(planet['BASE'][b])
	if 'STATION' in planet:
		my_print(2, "Stations:")
		for s in sorted(planet['STATION'].keys()):
			__print_station(planet['STATION'][s])

def __print_system(system:dict):
	my_print (0, "{0}", system['NAME'])
	for p in system['PLANET'].keys():
		__print_planet(system['PLANET'][p])

def print_all(systems:dict):
	for s in systems.keys():
		__print_system(systems[s])

def my_print(tab:int, msg, *args):
	print("\t"*tab, msg.format(*args))

def build():
	global SYSTEMS
	system = _build_nss()
	planet = _build_nva3(system)

	_build_base(planet, "N32", "Power Station", -14.9, -0.6, 0)
	_build_base(planet, "N87", "Launch Site", 54.6, 0, -508)
	_build_base(planet, "N94", "Power Station", 64.1, 0, 210)
	_build_base(planet, "N107", 'Launch Site', 88.6, 0, 732)
	_build_base(planet, "N133", "Launch Site", 132.5, 0, -366)
	_build_base(planet, "N163", "Launch Site", -153, 0, 1026)
	_build_base(planet, "N192", "Power Station", -85.8, -2.1, -689)

	_build_station(planet, 'ALCOTIM', 'Dispatcher', 0, 0, 758)
	_build_station(planet, 'AUKEN', 'Mine Class 4', 0, 0, 188)
	_build_station(planet, 'BECCA', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'CHRISTOS', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'COHAGEN', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'DACOSTA', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'DALBY', 'Dispatcher', 0, 0, 458)
	_build_station(planet, 'FENTON', 'Dispatcher', 0, 0, 758)
	_build_station(planet, 'GABA', 'Mine Class 5', 0, 0, 758)
	_build_station(planet, 'GALDAR', 'Dispatcher', 0, 0, 3058)
	_build_station(planet, 'GREENHOUSE', 'Mine Class 4', 0, 0, 188)
	_build_station(planet, 'HAYDEN', 'Mine Class 4', 0, 0, 188)
	_build_station(planet, 'MINE-A41', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'MONTANA', 'Dispatcher', 0, 0, 188)
	_build_station(planet, 'NORVIG', 'Mine Class 4', 0, 0, 758)
	_build_station(planet, 'PALMAS', 'Dispatcher', 0, 0, 458)
	_build_station(planet, 'SULLEN', 'Dispatcher', 0, 0, 1058)

def main(args:list):
	build()
	#pp = pprint.PrettyPrinter(depth=4)
	#pp.pprint(SYSTEMS)
	print_all(SYSTEMS)

if "__main__" == __name__ :
	main(sys.argv[1:])
	print ('End of line.')
