import sys

import NVA31
from NVA31 import SYSTEMS

def build():
	NVA31.build()
	planet = SYSTEMS['NOREN']['PLANET']['NVA-31']
	NVA31._build_base(planet, "N0", "Power Station", -132, 89.8, 17)
	NVA31._build_base(planet, "N48", "Power Station", 0.6, -2.1, -184)
	NVA31._build_base(planet, "N49", "Power Station", 1.6, -0.6, -58)
	NVA31._build_base(planet, "N51", "Power Station", 4.2, 3.9, 222)
	NVA31._build_base(planet, "N52", "Power Station", 4.5, 0.5, -631)
	NVA31._build_base(planet, "N55", "Power Station", 8.1, 1.5, -1382)

def main(args:list):
	build()
	NVA31.print_all(SYSTEMS)

if "__main__" == __name__ :
	main(sys.argv[1:])
	print ('End of line.')
